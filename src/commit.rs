use anyhow::{Context, Result};
use std::{
    fs::{read_to_string, write}
};

use super::{
    hook::get_git_absolute_path,
    prompt::prompt
};

pub fn check_rebase() -> Result<bool> {
    let path = get_git_absolute_path()?;

    if path.join("rebase-merge").exists() || path.join("rebase-apply").exists() {
        return Ok(true);
    }

    Ok(false)
}

pub fn generate_commit_message(commit_msg_file: &str) -> Result<()> {
    let existing_message = read_to_string(&commit_msg_file)
        .context(format!("Failed to read commit message file {:?}", &commit_msg_file))?;
    let commit_summary = prompt()?;
    let commit_message = format!("{}{}", commit_summary, existing_message);
    write(commit_msg_file, commit_message)
        .context(format!("Failed to write to commit message file {:?}", &commit_msg_file))?;
    Ok(())
}
