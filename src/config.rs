use anyhow::{Context, Result};
use serde::Deserialize;
use std::fs::read_to_string;

use super::hook::get_git_absolute_path;

const CONFIG_FILENAME: &str = ".lazyccrc";

#[derive(Deserialize)]
pub struct Config {
    pub commit_types: Option<Vec<String>>,
    pub scopes: Option<Vec<String>>
}

pub fn config() -> Result<Config> {
    let mut path = get_git_absolute_path()?;
    path.pop();
    path.push(CONFIG_FILENAME);
    if path.exists() {
        let config_str = read_to_string(&path)
            .context(format!("Failed to read config file {:?}", &path))?;
        let config: Config = toml::from_str(&config_str)
            .context(format!("Failed to parse config file {:?}", &path))?;
        return Ok(config);
    }
    Ok(Config { commit_types: None, scopes: None })
}
