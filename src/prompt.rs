use anyhow::Result;
use inquire::{max_length, min_length, Confirm, Select, Text};

use super::config::config;

const ITEMS: [&str; 10] = [
    "build: changes that affect the build system or external dependencies",
    "chore: changes to auxiliary tools and libraries",
    "ci: changes to CI configuration files and scripts",
    "docs: changes to the documentation",
    "feat: a new feature",
    "fix: a bug fix",
    "perf: a code change that improves performance",
    "refactor: a code change that neither fixes a bug nor adds a feature",
    "style: formatting, missing semi colons, etc; no production code change",
    "test: adding missing or correcting existing tests"
];

pub fn prompt() -> Result<String> {
    let config = config()?;
    let default_items = ITEMS.iter().map(|item| item.to_string()).collect();
    let items = match config.commit_types {
        Some(commit_types) => {
            match commit_types.len() {
                0 => default_items,
                _ => commit_types
            }
        },
        None => default_items
    };
    let commit_type = Select::new("What is the commit type?", items).prompt()?;
    let commit_type = commit_type.split(':').collect::<Vec<_>>()[0];

    let text_scope_prompt = Text::new("What is the commit scope?")
        .with_validator(max_length!(15, "Maximum of 15 characters"));
    let mut scope = match config.scopes {
        Some(scopes) => {
            match scopes.len() {
                0 => text_scope_prompt.prompt()?,
                _ => {
                    let scope = Select::new("What is the commit scope?", scopes).prompt()?;
                    scope.split(':').collect::<Vec<_>>()[0].to_string()
                }
            }
        },
        None => text_scope_prompt.prompt()?
    };

    let breaking_change = Confirm::new("Does it contain a breaking change?")
        .with_default(false)
        .prompt()?;

    let message = Text::new("Commit message?")
        .with_validator(min_length!(3, "Minimum of 3 characters"))
        .prompt()?;

    if !scope.is_empty() {
        scope = format!("({})", scope);
    }

    let is_breaking = match breaking_change {
        true => "!",
        false => "",
    };

    let commit_message = format!("{}{}{}: {}", commit_type, scope, is_breaking, message);

    Ok(commit_message)
}
